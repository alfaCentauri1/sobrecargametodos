package operaciones;

public class TestOperaciones {
    public static void main(String args[]){
        var suma = Operaciones.sumar(3, 5);
        System.out.printf("La suma es %d\n\n", suma);
        float suma2 = Operaciones.sumar(3.141516f, 2.5f);
        System.out.printf("La suma es %f\n", suma2);
        float suma3 = Operaciones.sumar(15, 2512345678L);
        System.out.printf("La suma es %f\n", suma3);
    }
}
